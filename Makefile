SUBDIRS := $(wildcard */)
SUBDIRS := $(filter-out a/, $(SUBDIRS))

$(info $$SUBDIRS is [${SUBDIRS}])

all: $(SUBDIRS) 

$(SUBDIRS):
	mkdir -p dist/$@
	$(MAKE) -C $@ clean
	$(MAKE) -C $@ all 
	cp -r $@/dist/* dist/$@
	echo "<div><a href=\"$@/index.html\">$@</li></div>" >> dist/index.html
	$(MAKE) -C $@ clean

clean:
	rm -rf dist/
