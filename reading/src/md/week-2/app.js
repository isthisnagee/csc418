const data = {
  scale: {
    uniform: true,
    x: 1,
    y: 1,
  },
  shear: {
    x: 0,
    y: 0,
  },
  rotation: {
    angle: 0,
  },
};

function squarePolygon(corner1, corner2) {
  return new Polygon(
    corner1,
    new Point(corner2.x, corner1.y),
    corner2,
    new Point(corner1.x, corner2.y),
  );
}

function windowTranslateToPoint(square, point) {
  return square.translateToPoint(point);
}

function shear_y(first = false) {
  const canvas = document.getElementById("shear-y");
  const ctx = canvas.getContext("2d");
  if (first) {
    ctx.translate(0, canvas.height);
    ctx.scale(1, -1);
  }
  // 0,0 is on the bottom left
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  /*
4 (0,1)  |<-----------^ 3. (1,1)
         |            |
         |            |
         v            |
 1.(0,0) -------------> 2. (0,1)
     */
  const square = new Polygon(
    new Point(0, 0),
    new Point(0, 50),
    new Point(50, 50),
    new Point(50, 0),
  );
  square.transform(Transformation.shear_y(this.shear.y), {
    getNew: false,
  });
  square.draw(ctx, {});
}

function shear_x(first = false) {
  const canvas = document.getElementById("shear-x");
  const ctx = canvas.getContext("2d");
  if (first) {
    ctx.translate(0, canvas.height);
    ctx.scale(1, -1);
  }
  // 0,0 is on the bottom left
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  /*
4 (0,1)  |<-----------^ 3. (1,1)
         |            |
         |            |
         v            |
 1.(0,0) -------------> 2. (0,1)
     */
  const square = new Polygon(
    new Point(0, 0),
    new Point(0, 50),
    new Point(50, 50),
    new Point(50, 0),
  );
  square.transform(Transformation.shear_x(this.shear.x), {
    getNew: false,
  });
  square.draw(ctx, {});
}

function rotate(first = false) {
  const canvas = document.getElementById("rotation");
  const ctx = canvas.getContext("2d");
  if (first) {
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.scale(1, -1);
  }
  // 0,0 is on the middle
  ctx.clearRect(
    -canvas.width,
    -canvas.height,
    canvas.width * 2,
    canvas.height * 2,
  );
  /*
4 (0,1)  |<-----------^ 3. (1,1)
         |            |
         |            |
         v            |
 1.(0,0) -------------> 2. (0,1)
     */
  const square = new Polygon(
    new Point(0, 0),
    new Point(0, 50),
    new Point(50, 50),
    new Point(50, 0),
  );
  square.transform(Transformation.rotate(this.rotation.angle), {
    getNew: false,
  });
  square.draw(ctx, {});
}

function scale(first = false) {
  const canvas = document.getElementById("scale");
  const ctx = canvas.getContext("2d");
  if (first) {
    ctx.translate(0, canvas.height);
    ctx.scale(1, -1);
  }
  // 0,0 is on the bottom left
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  /*
4 (0,1)  |<-----------^ 3. (1,1)
         |            |
         |            |
         v            |
 1.(0,0) -------------> 2. (0,1)
     */
  const square = new Polygon(
    new Point(0, 0),
    new Point(0, 50),
    new Point(50, 50),
    new Point(50, 0),
  );
  square.transform(Transformation.scale(this.scale.x, this.scale.y), {
    getNew: false,
  });
  square.draw(ctx, {});
}

Vue.component("range-transforms", {
  props: ["scale", "shear", "rotation"],
  computed: {
    scale_x: {
      get() {
        return this.scale.x;
      },
      set(v) {
        if (this.scale.uniform) {
          this.scale.y = v;
        }
        this.scale.x = v;
      },
    },
    scale_y: {
      get() {
        return this.scale.y;
      },
      set(v) {
        if (this.scale.uniform) {
          this.scale.x = v;
        }
        this.scale.y = v;
      },
    },
  },
  methods: {
    reset: function() {
      this.scale.uniform = true;
      this.scale.x = 1;
      this.scale.y = 1;
      this.shear.x = 0;
      this.shear.y = 0;
      this.rotation.angle = 0;
    },
  },

  template: `
<div class="dib">
  <button v-on:click="reset">Reset</button>
  <div class="ba pa2">
    <input type="checkbox" id="checkbox" v-model="scale.uniform">
    <label for="checkbox">uniform</label>
    <div>
      scale x:
      <input v-model="scale_x"
             min="0"
             max="2"
             step="0.1"
             type="range">
    </div>
    <div>
      scale y:
      <input v-model="scale_y" min="0" max="2" step="0.1" type="range" />
    </div>
  </div>
  <div>
    <div>
      shear y:
      <input v-model="shear.y"
             min="0"
             max="2"
             step="0.1"
             type="range">
    </div>
    <div>
      shear x:
      <input v-model="shear.x"
             min="0"
             max="2"
             step="0.1"
             type="range">
    </div>
  </div>
  <div>
    rotation angle:
    <input v-model="rotation.angle"
           min="0"
           max="6.28"
           list="pi"
           type="range"
           step="0.01"
          > 
  <datalist id="pi">
    <option value="0" label="0&pi;">
    <option value="1.57" label="&pi;/2">
    <option value="3.14" label="&pi;">
    <option value="4.71" label="3&pi;/4">
    <option value="6.28" label="2&pi;">
  </datalist>
  </div>
</div>
  `,
});

Vue.component("canvas-transforms", {
  props: ["scale", "shear", "rotation"],
  mounted: function() {
    scale.bind(this)(true);
    shear_x.bind(this)(true);
    shear_y.bind(this)(true);
    rotate.bind(this)(true);
  },
  template: `
    <div>
     <canvas style="display: inline-block; border: 1px solid black"
             id="scale"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="shear-x"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="shear-y"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="rotation"></canvas>
    </div>
  `,
  watch: {
    "scale.x": function() {
      scale.bind(this)(false);
    },
    "scale.y": function() {
      scale.bind(this)(false);
    },
    "shear.x": function() {
      shear_x.bind(this)(false);
    },
    "shear.y": function() {
      shear_y.bind(this)(false);
    },
    "rotation.angle": function() {
      rotate.bind(this)(false);
    },
  },
});

Vue.component("matrix-transforms", {
  props: ["scale", "shear", "rotation"],
  template: `
<div class="">
    <table  class="dib ml3 ph2 matrix">
        <tr>
            <td>{{scale.x}}</td>
            <td>0</td>
        </tr>
        <tr>
            <td>0</td>
            <td>{{scale.y}}</td>
        </tr>
    </table>
    <table class="dib ph2 ml3 matrix">
        <tr>
            <td>1</td>
            <td>{{shear.y}}</td>
        </tr>
        <tr>
            <td>0</td>
            <td>1</td>
        </tr>
    </table>
    <table class="dib ph2 ml3 matrix">
        <tr>
            <td>1</td>
            <td>0</td>
        </tr>
        <tr>
            <td>{{shear.x}}</td>
            <td>1</td>
        </tr>
    </table>
    <table class="dib ph2 ml3 matrix">
        <tr>
            <td>cos({{Math.floor(rotation.angle* (180 / Math.PI))}}&deg;)</td>
            <td>-sin({{Math.floor(rotation.angle* (180 / Math.PI))}}&deg;)</td>
        </tr>
        <tr>
            <td>sin({{Math.floor(rotation.angle* (180 / Math.PI))}}&deg;)</td>
            <td>cos({{Math.floor(rotation.angle* (180 / Math.PI))}}&deg;)</td>
        </tr>
    </table>
</div>
  `,
});

Vue.component("transforms", {
  props: ["scale", "shear", "rotation"],
  template: `
  <div class="cf">
  <div class="fl w-100 w-50-l">
  <range-transforms
    v-bind:scale="scale"
    v-bind:shear="shear"
    v-bind:rotation="rotation">
  </range-transforms>
  <matrix-transforms
    v-bind:scale="scale"
    v-bind:shear="shear"
    v-bind:rotation="rotation">
  </matrix-transforms>
  </div>
  <div class="fl w-100 w-50-l">
  <canvas-transforms
    v-bind:scale="scale"
    v-bind:shear="shear"
    v-bind:rotation="rotation">
  </canvas-transforms>
  </div>
  </div>
  `,
});

Vue.component("range-window-transforms", {
  props: ["a", "b", "A", "B"],
  template: `<div>{{a}}</div>`,
});
Vue.component("matrix-window-transforms", {
  props: ["a", "b", "A", "B"],
  template: `<div></div>`,
});

function canvasWindowTransforms(first = false) {
  const getCtx = id => {
    const canvas = document.getElementById(id);
    return canvas.getContext("2d");
  };
  const ctxs = {
    og: getCtx("original"),
    translateOG: getCtx("translate-original"),
    scaleOG: getCtx("scale-original"),
    translateFinal: getCtx("translate-final"),
  };
  if (first) {
    Object.keys(ctxs).map(ctx => {
      ctxs[ctx].translate(0, ctxs[ctx].canvas.height);
      ctxs[ctx].scale(1, -1);
    });
  }

  const og = squarePolygon(
    new Point(this.pa, this.pb),
    new Point(this.A, this.B),
  );

  const ogTranslated = og.translateToPoint(new Point(-this.pa, -this.pb), {
    getNew: true,
  });

  og.draw(ctxs.og, {});
  ogTranslated.draw(ctxs.translateOG, {});
}
Vue.component("canvas-window-transforms", {
  props: ["pa", "pb", "A", "B"],
  mounted: function() {
    canvasWindowTransforms.bind(this)(true);
  },
  template: `
    <div>
     <canvas style="display: inline-block; border: 1px solid black"
             id="original"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="translate-original"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="scale-original"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="translate-final"></canvas>
    </div>
  `,
  watch: {
    a: function() {
      canvasWindowTransforms.bind(this)(false);
    },
    b: function() {
      canvasWindowTransforms.bind(this)(false);
    },
    A: function() {
      canvasWindowTransforms.bind(this)(false);
    },
    B: function() {
      canvasWindowTransforms.bind(this)(false);
    },
  },
});

Vue.component("canvas-transforms", {
  props: ["scale", "shear", "rotation"],
  mounted: function() {
    scale.bind(this)(true);
    shear_x.bind(this)(true);
    shear_y.bind(this)(true);
    rotate.bind(this)(true);
  },
  template: `
    <div>
     <canvas style="display: inline-block; border: 1px solid black"
             id="scale"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="shear-x"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="shear-y"></canvas>
     <canvas style="display: inline-block; border: 1px solid black"
             id="rotation"></canvas>
    </div>
  `,
  watch: {
    "scale.x": function() {
      scale.bind(this)(false);
    },
    "scale.y": function() {
      scale.bind(this)(false);
    },
    "shear.x": function() {
      shear_x.bind(this)(false);
    },
    "shear.y": function() {
      shear_y.bind(this)(false);
    },
    "rotation.angle": function() {
      rotate.bind(this)(false);
    },
  },
});
// <range-window-transforms
//   v-bind:pa="pa"
//   v-bind:pb="pb"
//   v-bind:A="A"
//   v-bind:B="B">
// </range-window-transforms>
// <matrix-window-transforms
//   v-bind:pa="pa"
//   v-bind:pb="pb"
//   v-bind:A="A"
//   v-bind:B="B">
// </matrix-window-transforms>
// </div>
Vue.component("window-transforms", {
  props: ["pa", "pb", "A", "B"],
  template: `
  <div class="cf">
  <div class="fl w-100 w-50-l">
  <div class="fl w-100 w-50-l">
  <canvas-window-transforms
    v-bind:pa="pa"
    v-bind:pb="pb"
    v-bind:A="A"
    v-bind:B="B">
  </canvas-window-transforms>
  </div>
  </div>
  `,
});

var transforms = new Vue({
  el: "#transforms",
  data: data,
});

var windowTransforms = new Vue({
  el: "#windowing",
  data: {
    pa: 10,
    pb: 20,
    A: 30,
    B: 50,
  },
});
