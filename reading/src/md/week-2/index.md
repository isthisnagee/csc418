---
title: week-2
shared-js:
  - vue
  - csc418
js:
  - app
shared-css:
  - all.css
  - solarized.css
  - tachyons.css
css:
  - app.css
---

# Basic 2D Transforms

## Visualization

<div id="transforms" class=""><transforms v-bind:scale="scale" v-bind:shear="shear" v-bind:rotation="rotation"></transforms></div>

## Notes

![Derivation of rotation](./rotation.png "derivation of rotation")

Consider the image above. If we take $a$ and break it down into it's components,
we get that $x_a = r\cos(\alpha)$, and $y_a = r\sin(\alpha)$, where $r$ is the
length of the vector. $b$ has angle $\phi + \alpha$, and maintains length $r$.

$$
\begin{aligned}
x_b &= r\cos(\alpha + \theta) \\
    &= r\cos(\alpha)\cos(\theta) - r\sin(\alpha)\sin(\theta) \\
    &= x_a\cos(\theta) - y_a\sin(\theta)
\end{aligned}
$$
Similarly for the $y$ component of $b$:
$$
\begin{aligned}
y_b &= r\sin(\alpha + \theta) \\
    &= r\sin(\alpha)\cos(\theta) + r\cos(\alpha)\sin(\theta) \\
    &= y_a\cos(\theta) + x_a\sin(\theta) \\
    &= x_a\sin(\theta) + y_a\cos(\theta) \\
\end{aligned}
$$

Combining these two results, we get

 $$
x_b = x_a\cos(\theta) - y_a\sin(\theta) \\
y_b = x_a\sin(\theta) + y_a\cos(\theta)
$$

In matrix form, we can write this as:

$$
\begin{bmatrix}
x_b \\
y_b
\end{bmatrix}
=
\begin{bmatrix}
\cos(\theta) & -\sin(\theta) \\
\sin(\theta) &  \cos(\theta)
\end{bmatrix}
\begin{bmatrix}
x_a \\
y_a
\end{bmatrix}
$$
# Translations

To move locations, from $(x,y)$ to $(x', y')$, we need a translation that looks
like

$$
\begin{aligned}
x' = x + x_t
x' = x + y_t
\end{aligned}
$$

But we do not want _direction_ or _arbitrary offset vectors_ to move when we
apply this translation, so we move up to a __higher dimension__ to get this
done.

$$
\begin{bmatrix}
1 & 0 & x_t \\
0 & 1 & y_t \\
0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
x \\
y \\
0
\end{bmatrix}
=
\begin{bmatrix}
x + x_t \\
y + y_t \\
0
\end{bmatrix}
$$

### Windowing Transforms

<div id="windowing"><window-transforms v-bind:pa="pa" v-bind:pb="pb" v-bind:A="A" v-bind:B="B"></window-transofrms></div>

