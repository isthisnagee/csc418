const X = 0;
const Y = 1;
class Point3D {
  constructor(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  indexed() {
    return [this.x, this.y, this.z];
  }

  get2D({ getArray = false }) {
    if (getArray) return [this.x, this.y];
    return new Point(this.x, this.y);
  }
}
class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  static fromArray(p) {
    return new Point(p[X], p[Y]);
  }

  transform(T, { getNew = true }) {
    const p = this.indexed();
    const q = T.reduce((points, row) => {
      points.push(row.reduce((point, t, i) => (point += t * p[i]), 0));
      return points;
    }, []);
    const point = Point.fromArray(q);
    if (getNew) {
      return point;
    } else {
      this.x = point.x;
      this.y = point.y;
    }
  }

  translateToPoint(point, { getNew = true }) {
    return new Point3D(this.x + point.x, this.y + point.y, 1);
  }

  /**
   * Get the point as an array [x, y]
   * @return {number[]}
   */
  indexed() {
    return [this.x, this.y];
  }

  rotateAbout(angle) {
    return rotationAboutPoint(angle, this.indexed());
  }
}

class Transformation {
  static translate(x, y) {
    return [[1, 0, x], [0, 1, y], [0, 0, 1]];
  }

  static compose(T1, T2) {
    var T_new = [[0, 0, 0], [0, 0, 0]];

    for (var i = 0; i < 2; i++) {
      for (var j = 0; j < 3; j++) {
        for (var k = 0; k < 3; k++) {
          T_new[i][j] += T2[i][k] * (k === 2 ? (j === 2 ? 1 : 0) : T1[k][j]);
        }
      }
    }

    return T_new;
  }

  static scale(x, y) {
    if (y === undefined || y === null) y = x;
    return [[x, 0], [0, y]];
  }

  static shear_x(s) {
    return [[1, s], [0, 1]];
  }
  static shear_y(s) {
    return [[1, 0], [s, 1]];
  }
  static rotate(a) {
    return [[Math.cos(a), -1 * Math.sin(a)], [Math.sin(a), Math.cos(a)]];
  }
  static refelct_x() {
    return [[-1, 0], [0, 1]];
  }
  static refelct_y() {
    return [[1, 0], [0, -1]];
  }
}
/**
 * A Polygon is a set of points { p1, ... , pn } with a line
 * from each p_i to p_{(i+1) % n}
 * TODO ensure ^ is correct
 *
 */
class Polygon {
  constructor(...points) {
    this.points = points;
  }

  indexed() {
    return this.points.map(point => point.indexed());
  }

  draw(ctx, { wait = 0, style = "black" }) {
    const poly = this.indexed();
    poly.forEach((point, i, points) => {
      const nextPoint = points[(i + 1) % points.length];
      const p1 = Point.fromArray(point);
      const p2 = Point.fromArray(nextPoint);
      const lineSegment = new LineSegment(p1, p2);

      // for every i, wait i seconds, then draw.
      // if we do setTimeout(draw, 1000), then it will
      // draw all points after 1 seconds, instead of incrementally
      if (wait === 0) {
        lineSegment.draw(ctx, { wait, style });
      } else {
        setTimeout(() => {
          lineSegment.draw(ctx, { wait, style });
        }, (wait + i - 1) * 1000);
      }
    });
  }

  transform(T, { getNew = true }) {
    const newPoints = this.points.map(point => point.transform(T, {}));
    if (getNew) return newPoints;
    else this.points = newPoints;
  }

  translateToPoint(point, { getNew = true }) {
    const newPoints = this.points.map(point =>
      point.translateToPoint(point, { getNew: true }).get2D({}),
    );
    if (getNew) return new Polygon(...newPoints);
    else this.points = newPoints;
  }
}

class LineSegment {
  constructor(p1, p2) {
    this.start = p1;
    this.end = p2;
  }

  draw(ctx, { style = "black", wait = 0 }) {
    const [x1, y1] = this.start.indexed();
    const [x2, y2] = this.end.indexed();
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.strokeStyle = style;
    ctx.stroke();
  }
}

class Circle {
  constructor(centerPoint, radius) {
    this.center = centerPoint;
    this.radius = radius;
  }

  draw(ctx) {
    // TODO
    // drawCircle(ctx, ...this.center.indexed(), this.radius);
  }
}
